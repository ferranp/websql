from ConfigParser import ConfigParser
import os
import csv
from StringIO import StringIO

from flask import Flask,g
from flask import render_template
from flask import request,url_for
from flask import jsonify,Response

app = Flask(__name__)

databases_file = os.path.join(os.path.dirname(__file__),'databases.ini')
defaults = {
    'name':'','host':'','username':'','password':'',
}
databases = ConfigParser(defaults)
databases.read([databases_file])

def connect_sqlite(name,host,username,password):
    import sqlite3
    return sqlite3.connect(name)

def connect_mysql(name,host,username,password):
    import MySQLdb
    return MySQLdb.connect(host = host,
                           user = username,
                           passwd = password,
                           db = name)
DRIVERS = {
    'sqlite' : connect_sqlite,
    'mysql' : connect_mysql,
}

def connect_db(database):
    driver = databases.get(database,'driver')
    name = databases.get(database,'name')
    host = databases.get(database,'host')
    username = databases.get(database,'username')
    password = databases.get(database,'password')
    
    return DRIVERS[driver](name,host,username,password)

def return_csv(cursor):
    outfile = StringIO()
    out = csv.writer(outfile)
    columns = [i[0] for i in cursor.description]
    out.writerow(columns)
    for row in cursor.fetchall():
        out.writerow(row)
        
    response = Response(outfile.getvalue())
    response.mimetype = 'text/csv'
    return response

def return_json(cursor):
    columns = [i[0] for i in cursor.description]
    rows = []
    for row in cursor.fetchall():
        rowdict = {}
        for i,col in enumerate(columns):
            rowdict[col] = str(row[i]).decode('latin1')
        rows.append(rowdict)
    return jsonify(rows=rows)
    
FORMATS = {
    'csv': return_csv,
    'json': return_json,
}

@app.route("/")
def index():
    databases_list = []
    for db in databases.sections():
        databases_list.append((db,url_for('query',database=db)))

    return render_template('index.html', databases=databases_list)

@app.route('/<database>/query',methods=('GET','POST'))
def query(database):

    if request.method == 'POST':
        connection = connect_db(database)
        cursor = connection.cursor()
        cursor.execute(request.form['query'])
        format = request.form.get('format','csv')
        return FORMATS[format](cursor)
        
    return render_template('query.html', database=database)

if __name__ == "__main__":
    app.run(debug=True)

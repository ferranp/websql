
import sys
import os
sys.stdout = sys.stderr

sys.path.append(os.path.dirname(__file__))

from websql import app as application

import logging
file_handler = logging.StreamHandler()
file_handler.setLevel(logging.DEBUG)
application.logger.addHandler(file_handler)
